#include <iostream>
#include <ANN.h>
using namespace ANN;

// Is required .vs in .gitignore?

int main()
{
	//
	// Loading training data from the file
	//
	std::vector<std::vector<float>> inputs;
	std::vector<std::vector<float>> outputs;
	LoadData("xor.data", inputs, outputs);
	
	//
	// Creating a neural network with custom configuration
	//
	std::vector<size_t> configuration = { 2, 5, 5, 1 };
	ANeuralNetwork::ActivationType activationType = ANeuralNetwork::BIPOLAR_SYGMOID;
	std::shared_ptr<ANeuralNetwork> neuralNetwork = CreateNeuralNetwork(configuration, activationType);

	//
	// Training the neural network
	//
	float trainigError = BackPropTraining(neuralNetwork, inputs, outputs);
	std::cout << "Network was trained. Error is " << trainigError << std::endl;

	//
	// Saving trained neural network to the file
	//
	neuralNetwork->Save("xor.ann");

	return 0;
}