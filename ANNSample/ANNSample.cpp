#include <iostream>
#include <ANN.h>
using namespace ANN;

// How to create configuration??
// Manually or automatoic xor.ann moving?

int main()
{
	//
	// Creating a neural network with custom configuration
	//
	std::vector<size_t> configuration = { 2, 5, 5, 1 };
	ANeuralNetwork::ActivationType activationType = ANeuralNetwork::BIPOLAR_SYGMOID;
	std::shared_ptr<ANeuralNetwork> neuralNetwork = CreateNeuralNetwork(configuration, activationType);
	
	neuralNetwork->Load("xor.ann");

	std::cout << "Neural Network type: " << neuralNetwork->GetType() << std::endl;

	//
	// Loading training data from the file
	//
	std::vector<std::vector<float>> inputs;
	std::vector<std::vector<float>> outputs;
	LoadData("xor.data", inputs, outputs);

	std::cout << "x\ty\tx^y" << std::endl;

	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			std::vector<float> input = { (float)i, (float)j };
			std::vector<float> result = neuralNetwork->Predict(input);
			std::cout << i << "\t" << j << "\t" << round(result[0]) << "(" << result[0] << ")" << std::endl;
		}
	}

	return 0;
}